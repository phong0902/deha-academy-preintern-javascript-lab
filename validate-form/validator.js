function Validator(options) {
    var selectorRules = {};

    function validate(inputElement, rule) {
        var errorElement = inputElement.parentElement.querySelector(options.errorSelector);
        var errorMessage = rule.test(inputElement.value);

        if (errorMessage) {
            errorElement.innerText = errorMessage;
            inputElement.parentElement.classList.add('invalid');
        } else {
            errorElement.innerText = '';
            inputElement.parentElement.classList.remove('invalid');
        }

        return !errorMessage;
    }

    function attachInputEvent(inputElement, rule) {
        if (inputElement) {
            inputElement.onblur = function() {
                validate(inputElement, rule);
            };

            inputElement.oninput = function() {
                var errorElement = inputElement.parentElement.querySelector(options.errorSelector);
                errorElement.innerText = '';
                inputElement.parentElement.classList.remove('invalid');
            };
        }
    }

    var formElement = document.querySelector(options.form);
    if (formElement) {
        formElement.onsubmit = function(e) {
            e.preventDefault(); 
            var isFormValid = true;
            options.rules.forEach(function(rule) {
                var inputElement = formElement.querySelector(rule.selector);
                var isValid = validate(inputElement, rule);
                if (!isValid) {
                    isFormValid = false;
                }
            });

            
            if (isFormValid) {
                const result = document.getElementById("result");
                result.innerText = "Đăng ký thành công";
                result.style.textAlign = "center";
                result.style.color = "green";
            }
        };

        options.rules.forEach(function(rule) {
            var inputElement = formElement.querySelector(rule.selector);
            attachInputEvent(inputElement, rule);

            if (Array.isArray(selectorRules[rule.selector])) {
                selectorRules[rule.selector].push(rule.test);
            } else {
                selectorRules[rule.selector] = [rule.test];
            }
        });
    }
}

Validator.isRequired = function(selector,message) {
    return {
        selector: selector,
        test: function(value) {
            return value.trim() ? undefined : message||  'Vui lòng nhập trường này';
        }
    };
};

Validator.minLength = function(selector, min,message) {
    return {
        selector: selector,
        test: function(value) {
            return value.length == min ? undefined : message|| `Vui lòng nhập tối thiểu ${min} kí tự`;
        }
    };
};
